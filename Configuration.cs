﻿namespace DreamFruits.Discord
{
    internal abstract class Configuration
    {
        public const string DeploymentVersion = "v1.0.94-dfs";
        public const string DefaultPrefix = "-vs";
    }
}