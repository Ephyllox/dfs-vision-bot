﻿namespace DreamFruits.Discord
{
    internal enum Commands
    {
        REFRESH,
        PURGE,
        MDEL,
        PING,
        DPV,
    }
}