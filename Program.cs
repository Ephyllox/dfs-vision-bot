﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;

using dotenv.net;

using DreamFruits.Discord.Extensions;

using Pastel;

using Serilog;

using Color = System.Drawing.Color;
using IDictionary = System.Collections.IDictionary;

namespace DreamFruits.Discord
{
    public class Startup
    {
        public static void Main()
        {
            Console.WriteLine($"VISION BOT @ {Configuration.DeploymentVersion}\n".Pastel(Color.Gray));
            Console.WriteLine("Loading Vision...".Pastel(Color.LightGreen));

            ILogger logger = new LoggerConfiguration().WriteTo.Console().CreateLogger();
            Settings? settings;

            try
            {
                DotEnv.Load();
                IDictionary vars = Environment.GetEnvironmentVariables();

                settings = new()
                {
                    BotToken = vars["BOT_TOKEN"] as string,
                    Vision = new()
                    {
                        Prefix = vars["V_PREFIX"] as string ?? Configuration.DefaultPrefix,
                        MemberRole = ulong.Parse(vars["V_MEMBER_ROLE"] as string),
                        AdminUser = ulong.Parse(vars["V_ADMIN_USER"] as string),
                    },
                };
            }
            catch
            {
                logger.Error("CRITICAL >> Cannot start: failed to load env-vars.".Pastel(Color.OrangeRed));
                return;
            }

            _ = new VisionClient(settings, logger);

            Console.ReadKey();
        }
    }

    internal class VisionClient
    {
        private DiscordSocketClient? client;
        private readonly Settings _settings;
        private readonly ILogger _logger;

        public VisionClient(Settings settings, ILogger logger)
        {
            _settings = settings;
            _logger = logger;

            Login();
        }

        private bool AssertPermission(SocketGuildUser? user)
        {
            if (user == null)
                return false;

            return user.Id == _settings.Vision.AdminUser || user.GuildPermissions.Administrator;
            //return user.Roles.Any(role => role.Id is adminRoleId);
        }

        private async void CheckMessage(SocketMessage msg)
        {
            try
            {
                string filteredMsg = Regex.Replace(msg.CleanContent, "[^a-zA-Z_.]+", string.Empty, RegexOptions.Compiled).Replace(" ", string.Empty);

                if (filteredMsg.ContainsRegardless("discord.gg") || filteredMsg.ContainsRegardless("dlcsord.com"))
                {
                    await msg.DeleteAsync();
                    _logger.Warning($"Removed server link sent by: @{msg.Author.Username}".Pastel(Color.DarkOrange));
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Filter exception thrown: {ex}");
            }
        }

        private async void Login()
        {
            client = new(new()
            {
                GatewayIntents = GatewayIntents.All,
            });

            client.Ready += () =>
            {
                Console.WriteLine($"Vision has loaded! Prefix '{_settings.Vision.Prefix}' is used.\n".Pastel(Color.Green));

                return Task.CompletedTask;
            };

            client.MessageReceived += (msg) =>
            {
                if (!msg.Author.IsBot)
                {
                    Console.WriteLine($"{$"[{msg.Author.Username}#{msg.Author.Discriminator}]".Pastel(Color.SeaGreen)}: {msg.Content.Pastel(Color.LightSeaGreen)}");

                    if (msg.Content.StartsWith($"{_settings.Vision.Prefix} "))
                    {
                        if (AssertPermission(msg.Author as SocketGuildUser))
                            OnMessage(msg);
                        else
                            msg.Channel.SendMessageAsync("```Insufficient permissions.```");
                    }

                    if (!AssertPermission(msg.Author as SocketGuildUser))
                        CheckMessage(msg);
                }

                return Task.CompletedTask;
            };

            client.MessageUpdated += OnMessageUpdate;
            client.UserJoined += OnUserJoin;

            await client.LoginAsync(TokenType.Bot, _settings.BotToken);
            await client.StartAsync();
        }

        private async Task OnUserJoin(SocketGuildUser user)
        {
            if (!user.IsBot)
            {
                _logger.Information($"User joined: @{user.Username}#{user.Discriminator}".Pastel(Color.LawnGreen));
                await user.AddRoleAsync(_settings.Vision.MemberRole);
            }
        }

        private async void OnMessage(SocketMessage msg)
        {
            string[] args = msg.Content[4..].ToLower().Split(' ');

            try
            {
                if (Enum.TryParse(args[0].ToUpper(), out Commands cmd))
                {
                    switch (cmd)
                    {
                        case Commands.REFRESH:
                            SocketGuildChannel? channel = msg.Channel as SocketGuildChannel;

                            _logger.Information("Downloading users...".Pastel(Color.LightGreen));

                            await channel?.Guild.DownloadUsersAsync();

                            _logger.Information("Download complete!".Pastel(Color.LightGreen));
                            _logger.Debug($"User count: {channel?.Guild.Users.Count}".Pastel(Color.LightGreen));

                            IEnumerable<SocketGuildUser?>? users = Array.Empty<SocketGuildUser?>();

                            if (args[1] is "all")
                                users = channel?.Guild.Users.ToArray();
                            else
                                users = users.Append(channel?.Guild.Users.Single(user => user.Id == ulong.Parse(args[1])));

                            foreach (SocketGuildUser? user in users)
                            {
                                bool isMember = user.Roles.Any(role => role.Id == _settings.Vision.MemberRole);

                                if (!user.IsBot && !isMember)
                                {
                                    await Task.Delay(100);
                                    _logger.Information($"Adding default role to: @{user.Username}#{user.Discriminator}".Pastel(Color.Azure));
                                    await user.AddRoleAsync(_settings.Vision.MemberRole);
                                }
                                else if (user.IsBot && isMember)
                                {
                                    _logger.Warning($"Removing role from bot: @{user.Username}".Pastel(Color.Azure));
                                    await user.RemoveRoleAsync(_settings.Vision.MemberRole);
                                }
                            }

                            _logger.Information("Refresh complete!".Pastel(Color.LightGreen));
                            break;
                        case Commands.PURGE:
                            int count = int.Parse(args[1]);

                            if (count > 0)
                            {
                                IEnumerable<IMessage> msgs = await msg.Channel.GetMessagesAsync(count < 80 ? count : 80, CacheMode.AllowDownload).FlattenAsync();

                                if (args.Length is 3)
                                    msgs = msgs.Where(msg => msg.Author.Id == ulong.Parse(args[2]));

                                _logger.Warning(($"Purge requested: {count} message(s) in #{msg.Channel.Name}" +
                                    $"{(args.Length is 3 ? $", from @{args[2]}" : null)}").Pastel(Color.LightGreen));

                                await (msg.Channel as SocketTextChannel).DeleteMessagesAsync(msgs);
                                _logger.Information("Purge complete!".Pastel(Color.LightGreen));
                            }

                            break;
                        case Commands.MDEL:
                            ulong target = ulong.Parse(args[1]);
                            await msg.Channel.DeleteMessageAsync(target);
                            break;
                        case Commands.PING:
                            await msg.Channel.SendMessageAsync($"```{client.Latency}ms```");
                            break;
                        case Commands.DPV:
                            await msg.Channel.SendMessageAsync($"```{Configuration.DeploymentVersion}```");
                            break;
                    }
                }
                else
                {
                    string list = string.Join(", ", Enum.GetNames(typeof(Commands)));
                    await msg.Channel.SendMessageAsync($"```Command not found. Available: [{list}]```");
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"PROC-ERR :: {ex}".Pastel(Color.OrangeRed));
                await msg.Channel.SendMessageAsync("```A process error has occurred.```");
            }
        }

        private Task OnMessageUpdate(Cacheable<IMessage, ulong> _old, SocketMessage msg, ISocketMessageChannel _channel)
        {
            CheckMessage(msg);
            return Task.CompletedTask;
        }
    }
}