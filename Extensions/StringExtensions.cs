﻿using System;

namespace DreamFruits.Discord.Extensions
{
    internal static class StringExtensions
    {
        public static bool ContainsRegardless(this string input, string text)
        {
            return input.Contains(text, StringComparison.OrdinalIgnoreCase);
        }
    }
}