﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Threading.Tasks;

using CSystem.Runtime;
using CSystem.Controllers;

using SST.Navigation;
using SST.Navigation.Models;
using SST.Navigation.Interfaces;
using SST.Navigation.Constants;

using SST.Security;
using SST.Security.Models;
using SST.Security.Interfaces;
using SST.Security.Constants;

namespace CSystem
{
    public class Program
    {
        public static void Main()
        {
            new Runtime.Runtime();
            LoginProcess();
        }

        public static void LoginProcess()
        {
            Console.Clear();
            Console.WriteLine("ConsoleSpaces @ v0.2");
            Console.WriteLine("\nEnter username:");
            string name = Console.ReadLine();

            Console.WriteLine($"\nEnter password for {name}:");
            string pass = Console.ReadLine();

            User user = RuntimeData.Users.Find(item => item.Username == name && item.Password == pass);

            if (user != null)
            {
                Console.WriteLine("\n= User found =\n");
                Console.WriteLine("= Logging in... =");
                //await Task.Delay(1000);
                OnLogin(user);
            }
            else
            {
                //await Task.Delay(1000);
                Console.WriteLine("\nThat username and password combination is not registered!");
                Console.ReadKey();
                LoginProcess();
            }
        }

        public static void OnLogin(User user)
        {
            Console.Clear();
            Console.WriteLine("Welcome!");
            Console.WriteLine($"You're logged in as: @{user.Username}.\n");
            Console.WriteLine($"## The date now is {DateTime.Now} ##\n");
            Console.WriteLine($"Available spaces for *{user.Grants.Max().ToString().ToLower()}*: [{Utils.GetAllNavigationRoutes(user)}]\n");
            Console.WriteLine("-- Enter route:");

            string input = Console.ReadLine();
            NavigationRoute route = RuntimeData.Routes.Find(item => item.Name.ToLower() == input.ToLower()
                || item.Aliases.Find(sub => sub.ToLower() == input.ToLower()) != null);

            if (route != null)
            {
                Console.WriteLine($"\nNavigating to \"{route.Name}\"...");
                //await Task.Delay(250);

                if (!route.AssertUserAccess(user))
                {
                    Console.WriteLine("Sorry - you do not have sufficient access to navigate to that space.");
                    Console.ReadKey();
                    OnLogin(user);
                    return;
                }

                Console.Clear();

                string intro = $"You're currently at: \"{route.Name}\"";
                Console.WriteLine($"{intro}\n{new string('-', intro.Length - intro.Length / 8)}\n");

                RouteResult? result = route.OnInvoke(user);

                switch (result)
                {
                    case RouteResult.RETURN:
                        Console.ReadKey();
                        break;
                    case RouteResult.EXCEPTION:
                        Console.Clear();
                        Console.WriteLine("An error occurred during navigation.");
                        Console.WriteLine("Press any key to return.");
                        Console.ReadKey();
                        break;
                }

                OnLogin(user);
            }
            else
            {
                Console.WriteLine("\nRoute invalid!");
                Console.ReadKey();
                OnLogin(user);
            }
        }
    }

    public static class Utils
    {
        public static string GetAllNavigationRoutes(User user)
        {
            string final = string.Empty;
            int index = 0;

            IEnumerable<NavigationRoute> routes = RuntimeData.Routes.Where(item => item.AssertUserAccess(user));

            foreach (NavigationRoute route in routes)
            {
                index++;
                final += $"{route.Name}" + (index != routes.Count() ? ", " : string.Empty);
            }

            return final;
        }
    }
}

namespace CSystem.Runtime
{
    public class Runtime
    {
        public Runtime()
        {
            RegisterRoutes();
        }

        private static void RegisterRoutes()
        {
            IEnumerable<MethodInfo> methods = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(x => x.GetTypes())
                    .Where(x => x.IsClass && x.BaseType == typeof(BaseController))
                    .SelectMany(x => x.GetMethods())
                    .Where(x => x.GetCustomAttributes(typeof(NavigationRouteAttribute), false).FirstOrDefault() != null);

            foreach (MethodInfo method in methods)
            {
                object obj = Activator.CreateInstance(method.DeclaringType);

                NavigationRouteAttribute route = (NavigationRouteAttribute)
                    method.GetCustomAttributes(typeof(NavigationRouteAttribute), false).FirstOrDefault();
                NavigationRouteAliasAttribute names = (NavigationRouteAliasAttribute)
                    method.GetCustomAttributes(typeof(NavigationRouteAliasAttribute), false).FirstOrDefault();
                NavigationRoutePolicyAttribute policy = (NavigationRoutePolicyAttribute)
                    method.GetCustomAttributes(typeof(NavigationRoutePolicyAttribute), false).FirstOrDefault();

                if (RuntimeData.Routes.Find(item => item.Name.ToLower() == route.Name.ToLower()) != null)
                {
                    throw new Exception("Route already exists in runtime data.");
                }

                RuntimeData.Routes.Add(new NavigationRoute()
                {
                    Name = route.Name,
                    Aliases = new List<string>(names?.Aliases ?? new string[0]),
                    OnInvoke = (User user) =>
                    {
                        try
                        {
                            ParameterInfo[] parameters = method.GetParameters();
                            object[] input = null;

                            foreach (ParameterInfo par in parameters)
                            {
                                input = input ?? new object[parameters.Length];

                                if (par.ParameterType == typeof(User))
                                {
                                    input[0] = user;
                                }
                            }

                            return (RouteResult?)method.Invoke(obj, input);
                        }
                        catch
                        {
                            return RouteResult.EXCEPTION;
                        }
                    },
                    NavPolicy = policy != null ? new NavigationPolicy(policy.Grants) : null,
                });
            }
        }
    }

    public static class RuntimeData
    {
        public static readonly List<User> Users = new List<User>()
        {
            new User("admin", "pass", new UserGrants[] {
                UserGrants.SUPERUSER,
            }),
            new User("test", "user", new UserGrants[] {
                UserGrants.DEFAULT_ACCESS,
            }),
        };

        public static readonly List<NavigationRoute> Routes = new List<NavigationRoute>();
    }
}

#region Controllers
namespace CSystem.Controllers
{
    public class CoreController : BaseController
    {
        [NavigationRoute("exit")]
        public RouteResult? Exit()
        {
            Environment.Exit(0);
            return Done();
        }

        [NavigationRoute("logout")]
        public RouteResult? Logout()
        {
            Program.LoginProcess();
            return Done();
        }
    }

    public class SystemController : BaseController
    {
        [NavigationRoute("adduser")]
        [NavigationRouteAlias("adu")]
        [NavigationRoutePolicy(UserGrants.SUPERUSER)]
        public RouteResult? AddUser()
        {
            Console.WriteLine("[-] Enter new user name:");
            string name = Console.ReadLine();

            if (RuntimeData.Users.Find(user => user.Username == name) != null)
            {
                Console.WriteLine("\n[ ! ] That user already exists.");
                return Return();
            }

            Console.WriteLine("[-] Enter new user password:");
            string pass = Console.ReadLine();

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(pass))
            {
                Console.WriteLine("\n[ ! ] You need to enter a valid username and password.");
                return Return();
            }

            int grants = Enum.GetValues(typeof(UserGrants)).Length;
            Console.WriteLine($"[-] Enter user permission level (1 - {grants}):");

            if (!int.TryParse(Console.ReadLine(), out int grant) || !Enum.IsDefined(typeof(UserGrants), grant - 1))
            {
                Console.WriteLine("\n[ ! ] You need to enter a valid user permission level.");
                return Return();
            }

            RuntimeData.Users.Add(new User(name, pass, new UserGrants[] {
                (UserGrants)grant - 1,
            }));

            Console.WriteLine($"\nSuccessfully created account. Press any key to return.");
            return Return();
        }

        [NavigationRoute("removeuser")]
        [NavigationRouteAlias("rmu")]
        [NavigationRoutePolicy(UserGrants.SUPERUSER)]
        public RouteResult? RemoveUser(User identity)
        {
            Console.WriteLine("[-] Enter user name:");
            string name = Console.ReadLine();

            User target = RuntimeData.Users.Find(user => user.Username == name);

            if (target != null)
            {
                if (target.Username == identity.Username)
                {
                    Console.WriteLine("\n[ ! ] You cannot remove your own account.");
                    return Return();
                }

                if (target.Grants.Max() >= identity.Grants.Max())
                {
                    Console.WriteLine("\n[ ! ] The specified user has a higher permission level.");
                    return Return();
                }

                RuntimeData.Users.Remove(target);

                Console.WriteLine("\nSuccessfully removed account.");
                return Return();
            }

            Console.WriteLine("\n[ ! ] The specified user does not exist.");
            return Return();
        }

        [NavigationRoute("changepassword")]
        [NavigationRouteAlias("chpass")]
        [NavigationRoutePolicy(UserGrants.ADMIN_ACCESS)]
        public RouteResult? ChangePassword(User identity)
        {
            Console.WriteLine("[-] Enter user name:");
            string name = Console.ReadLine();

            User target = RuntimeData.Users.Find(user => user.Username == name);

            if (target != null)
            {
                if (target.Grants.Max() > identity.Grants.Max())
                {
                    Console.WriteLine("\n[ ! ] The specified user has a higher permission level.");
                    return Return();
                }

                Console.WriteLine("[-] Enter updated user password:");
                string pass = Console.ReadLine();

                if (string.IsNullOrEmpty(pass))
                {
                    Console.WriteLine("\n[ ! ] You need to enter a valid password.");
                    return Return();
                }

                target.Password = pass;

                Console.WriteLine("\nSuccessfully updated account password.");
                return Return();
            }

            Console.WriteLine("\n[ ! ] The specified user does not exist.");
            return Return();
        }
    }
}
#endregion

#region Infrastructure
namespace SST.Navigation
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class NavigationRouteAttribute : Attribute
    {
        public string Name { get; set; }

        public NavigationRouteAttribute(string name)
        {
            Name = name;
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class NavigationRouteAliasAttribute : Attribute
    {
        public string[] Aliases { get; set; }

        public NavigationRouteAliasAttribute(params string[] aliases)
        {
            Aliases = aliases;
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class NavigationRoutePolicyAttribute : Attribute
    {
        public UserGrants[] Grants { get; set; }

        public NavigationRoutePolicyAttribute(params UserGrants[] grants)
        {
            Grants = grants;
        }
    }
}

namespace SST.Navigation.Models
{
    public class NavigationPolicy
    {
        public NavigationPolicy(UserGrants[] grants)
        {
            Grants = grants;
        }

        public UserGrants[] Grants { get; set; }
    }

    public class NavigationRoute
    {
        public string Name { get; set; }

        public List<string> Aliases { get; set; }

        public Func<User, RouteResult?> OnInvoke { get; set; }

        public NavigationPolicy NavPolicy { get; set; }

        public bool AssertUserAccess(User user) =>
            this.NavPolicy == null || this.NavPolicy.Grants.All(item => user.Grants.Any(grant => item <= grant)) == true;
    }

    public class BaseController : IBaseController
    {
        public RouteResult Return()
        {
            return RouteResult.RETURN;
        }

        public RouteResult? Done()
        {
            return null;
        }
    }
}

namespace SST.Navigation.Interfaces
{
    public interface IBaseController
    {
        RouteResult Return();

        RouteResult? Done();
    }
}

namespace SST.Navigation.Constants
{
    public enum RouteResult
    {
        EXCEPTION,
        RETURN,
    }
}

namespace SST.Security
{

}

namespace SST.Security.Models
{
    public sealed class User
    {
        public User(string username, string password, UserGrants[] grants)
        {
            Username = username;
            Password = password;
            Grants = grants;
        }

        public string Username { get; set; }
        public string Password { get; set; }

        public UserGrants[] Grants { get; set; }
    }
}

namespace SST.Security.Interfaces
{

}

namespace SST.Security.Constants
{
    public enum UserGrants
    {
        DEFAULT_ACCESS,
        ADMIN_ACCESS,
        SUPERUSER,
    }
}
#endregion