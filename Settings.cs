﻿namespace DreamFruits.Discord
{
    internal class Settings
    {
        public string? BotToken { get; set; }

        public VisionConfig? Vision { get; set; }

        internal class VisionConfig
        {
            public string? Prefix { get; set; }

            public ulong MemberRole { get; set; } = default;

            public ulong AdminUser { get; set; } = default;
        }
    }
}